package ibu.edu.ba.sufara;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HarfFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HarfFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HarfFragment extends Fragment {

    private View harfView;

    public HarfFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.harfView = inflater.inflate(R.layout.fragment_harf, container, false);
        return this.harfView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String harf = this.getArguments().getString("harf_name");
        getActivity().setTitle(harf);

        TextView textView = (TextView) harfView.findViewById(R.id.textView);
        textView.setText(harf + "BELMIN");
    }

}
