package ibu.edu.ba.sufara;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        HarfFragment harf = new HarfFragment();
        Bundle b = new Bundle();

        if (id == R.id.elif) {
            b.putString("harf_name", "ELIF - \u0627");
        } else if (id == R.id.ba) {
            b.putString("harf_name", "BA - \u0628");
        } else if (id == R.id.ta) {
            b.putString("harf_name", "TA - \u062A");
        } else if (id == R.id.sa) {
            b.putString("harf_name", "SA - \u062B");
        } else if (id == R.id.dzim) {
            b.putString("harf_name", "DZIM - \u062C");
        } else if (id == R.id.ha) {
            b.putString("harf_name", "HA -  \u062d");
        } else if (id == R.id.ha2) {
            b.putString("harf_name", "ĤA -  \u062e");
        } else if (id == R.id.dad) {
            b.putString("harf_name", "DAD -  \u0636");
        } else if (id == R.id.sad) {
            b.putString("harf_name", "SAD -  \u0635");
        } else if (id == R.id.šin) {
            b.putString("harf_name", "ŠIN -  \u0634");
        } else if (id == R.id.sin) {
            b.putString("harf_name", "SIN -  \u0633");
        } else if (id == R.id.za) {
            b.putString("harf_name", "ZA -  \u0632");
        } else if (id == R.id.ra) {
            b.putString("harf_name", "RA -  \u0631");
        } else if (id == R.id.zal) {
            b.putString("harf_name", "ZAL -  \u062f");
        } else if (id == R.id.dal) {
            b.putString("harf_name", "DAL -  \u0630");
        } else if (id == R.id.lam) {
            b.putString("harf_name", "LAM -  \u0644");
        } else if (id == R.id.kaf) {
            b.putString("harf_name", "KAF -  \u0643");
        } else if (id == R.id.kaf2) {
            b.putString("harf_name", "KAF -  \u0642");
        } else if (id == R.id.fa) {
            b.putString("harf_name", "FA -  \u0641");
        } else if (id == R.id.gajn) {
            b.putString("harf_name", "GAJN -  \u063a");
        } else if (id == R.id.ajn) {
            b.putString("harf_name", "AJN -  \u0639");
        } else if (id == R.id.za2) {
            b.putString("harf_name", "ZA -  \u0638");
        } else if (id == R.id.ta2) {
            b.putString("harf_name", "TA - \u0637");
        } else if (id == R.id.ja) {
            b.putString("harf_name", "JA - \u0649");
        } else if (id == R.id.ha3) {
            b.putString("harf_name", "HA - \u0647");
        } else if (id == R.id.vav) {
            b.putString("harf_name", "VAV - \u0648");
        } else if (id == R.id.nun) {
            b.putString("harf_name", "NUN - \u0646");
        } else if (id == R.id.mim) {
            b.putString("harf_name", "MIM - \u0645");
        } else if (id == R.id.about) {
                // fali ti nesto ovdje
        }

        harf.setArguments(b);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, harf);
        ft.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
